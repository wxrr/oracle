# 实验5：包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

## 定义一个名为 "MyPack" 的包

```
create or replace PACKAGE MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
```

![pict1](pict1.png)

## 创建两个子程序

```
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
```

该包体中包含两个子程序：

1. 函数 "Get_SalaryAmount"：接收一个参数 "V_DEPARTMENT_ID"（部门ID），返回一个数值类型的结果。函数内部通过查询语句计算指定部门中员工的薪水总额，并将结果存储在变量 "N" 中，然后返回该变量的值。
2. 过程 "GET_EMPLOYEES"：接收一个参数 "V_EMPLOYEE_ID"（员工ID），不返回任何结果。过程内部使用递归查询（CONNECT BY PRIOR）从指定员工ID开始，逐级查询下属员工的信息，并通过DBMS_OUTPUT.PUT_LINE函数将结果输出到控制台。输出的信息包括员工ID、姓名和直接上级的ID。

![pict2](pict2.png)

## 测试

```
# 函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
```

![pict3](pict3.png)

```sh
函数Get_SalaryAmount()测试方法：
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
```

![pict4](pict4.png)

## 实验总结

- 在这个实验中，我们创建了一个包（Package）和包体（Package Body），其中包含了一个函数（Function）和一个过程（Procedure）。
- 在包中，定义了一个名为Get_SalaryAmount的函数，它接受一个部门ID作为参数，并返回该部门中所有员工的薪水总额。我们还定义了一个名为Get_Employees的过程，它接受一个员工ID作为参数，并打印出该员工及其下属的层级关系。
- 在测试函数Get_SalaryAmount时，我们使用了一个SELECT语句，通过调用MyPack.Get_SalaryAmount(department_id)来获取每个部门的薪水总额，并将结果作为salary_total列返回。
- 在测试过程Get_Employees时，我们使用了PL/SQL块，在块中声明了一个变量V_EMPLOYEE_ID并赋值为101，然后调用MYPACK.Get_Employees(V_EMPLOYEE_ID => V_EMPLOYEE_ID)来打印员工101及其下属的层级关系。
