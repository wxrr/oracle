﻿﻿# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

##### 姓名：王习蓉

##### 学号：202010414418

##### 班级：20级软工4班

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

### 实验内容

##### 1、建立表空间和用户

```
--表空间PROPERTY sale
CREATE TABLESPACE sale
	DATAFILE 'sale.dbf' 
	size 800M
    EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO;   
 
--2.建用户
create user sale identified by sale 
default tablespace sale;
 
--3.赋权
grant connect,resource to sale;
grant create any sequence to sale;
grant create any table to sale;
grant delete any table to sale;
grant insert any table to sale;
grant select any table to sale;
grant unlimited tablespace to sale;
grant execute any procedure to sale;
grant update any table to sale;

--表空间PROPERTY sale
CREATE TABLESPACE PROPERTY
	DATAFILE 'PROPERTY.dbf' 
	size 800M
    EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO; 

--2.建用户
create user PROPERTY identified by PROPERTY 
default tablespace PROPERTY;
 
--3.赋权
grant connect,resource to PROPERTY;
grant create any sequence to PROPERTY;
grant create any table to PROPERTY;
grant delete any table to PROPERTY;
grant insert any table to PROPERTY;
grant select any table to PROPERTY;
grant unlimited tablespace to PROPERTY;
grant execute any procedure to PROPERTY;
grant update any table to PROPERTY;
```

![pict1](pict1.png)

![pict2](pict2.png)

![pict3](pict3.png)

![pict4](pict4.png)

##### 2、在PROPERTY空间中建立表和索引

```
CREATE TABLE "PROPERTY"."goods_info" 
   (	"goods_id" NUMBER(20,0) NOT NULL ENABLE, 
	"goods_name" NVARCHAR2(200) NOT NULL ENABLE, 
	"price" NUMBER(11,0), 
	"stock_num" NUMBER(11,0) NOT NULL ENABLE, 
	"goods_sell_status" NVARCHAR2(30) NOT NULL ENABLE, 
	"create_user" NVARCHAR2(200), 
	"create_time" DATE, 
	"update_user" NVARCHAR2(200), 
	"update_time" DATE, 
	 CONSTRAINT "_COPY_6" PRIMARY KEY ("goods_id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "PROPERTY"  ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "PROPERTY" ;

CREATE UNIQUE INDEX "PROPERTY"."_COPY_6" ON "PROPERTY"."goods_info" ("goods_id") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "PROPERTY" ;

COMMENT ON COLUMN PROPERTY."goods_info"."goods_id" IS '商品表主键id';
COMMENT ON COLUMN PROPERTY."goods_info"."goods_name" IS '商品名';
COMMENT ON COLUMN PROPERTY."goods_info"."price" IS '商品实际售价';
COMMENT ON COLUMN PROPERTY."goods_info"."stock_num" IS '商品库存数量';
COMMENT ON COLUMN PROPERTY."goods_info"."goods_sell_status" IS '商品上架状态 0-下架 1-上架';
COMMENT ON COLUMN PROPERTY."goods_info"."create_user" IS '添加者主键id';
COMMENT ON COLUMN PROPERTY."goods_info"."create_time" IS '商品添加时间';
COMMENT ON COLUMN PROPERTY."goods_info"."update_user" IS '修改者主键id';
COMMENT ON COLUMN PROPERTY."goods_info"."update_time" IS '商品修改时间';

CREATE TABLE "PROPERTY"."order" 
   (	"order_id" NUMBER(20,0) NOT NULL ENABLE, 
	"user_id" NUMBER(20,0) NOT NULL ENABLE, 
	"total_price" NUMBER(11,0) NOT NULL ENABLE, 
	"pay_time" DATE, 
	"order_status" NUMBER(4,0) NOT NULL ENABLE, 
	"create_time" DATE NOT NULL ENABLE, 
	"update_time" DATE NOT NULL ENABLE, 
	 CONSTRAINT "_COPY_4" PRIMARY KEY ("order_id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "PROPERTY"  ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "PROPERTY" ;

CREATE UNIQUE INDEX "PROPERTY"."_COPY_4" ON "PROPERTY"."order" ("order_id") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "PROPERTY" ;

COMMENT ON COLUMN PROPERTY."order"."order_id" IS '订单表主键id';
COMMENT ON COLUMN PROPERTY."order"."user_id" IS '用户主键id';
COMMENT ON COLUMN PROPERTY."order"."total_price" IS '订单总价';
COMMENT ON COLUMN PROPERTY."order"."pay_time" IS '支付时间';
COMMENT ON COLUMN PROPERTY."order"."order_status" IS '订单状态:0.待支付 1.已支付 2.取消';
COMMENT ON COLUMN PROPERTY."order"."create_time" IS '创建时间';
COMMENT ON COLUMN PROPERTY."order"."update_time" IS '最新修改时间';

CREATE TABLE "PROPERTY"."order_item" 
   (	"order_item_id" NUMBER(20,0) NOT NULL ENABLE, 
	"order_id" NUMBER(20,0) NOT NULL ENABLE, 
	"goods_id" NUMBER(20,0) NOT NULL ENABLE, 
	"selling_price" NUMBER(11,0) NOT NULL ENABLE, 
	"goods_count" NUMBER(11,0) NOT NULL ENABLE, 
	"create_time" DATE NOT NULL ENABLE, 
	 CONSTRAINT "_COPY_3" PRIMARY KEY ("order_item_id")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "PROPERTY"  ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "PROPERTY" ;

CREATE UNIQUE INDEX "PROPERTY"."_COPY_3" ON "PROPERTY"."order_item" ("order_item_id") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "PROPERTY" ;

COMMENT ON COLUMN PROPERTY."order_item"."order_item_id" IS '订单关联购物项主键id';
COMMENT ON COLUMN PROPERTY."order_item"."order_id" IS '订单主键id';
COMMENT ON COLUMN PROPERTY."order_item"."goods_id" IS '关联商品id';
COMMENT ON COLUMN PROPERTY."order_item"."selling_price" IS '下单时商品的价格(订单快照)';
COMMENT ON COLUMN PROPERTY."order_item"."goods_count" IS '数量(订单快照)';
COMMENT ON COLUMN PROPERTY."order_item"."create_time" IS '创建时间';
```

![pict5](pict5.png)

![pict6](pict6.png)

![pict7](pict7.png)

##### 3、在SALE空间中建表

```
CREATE TABLE "shopping_cart" (
  "cart_item_id" NUMBER(20,0) NOT NULL,
  "user_id" NUMBER(20,0) NOT NULL,
  "goods_id" NUMBER(20,0) NOT NULL,
  "goods_count" NUMBER(11,0) NOT NULL,
  "create_time" DATE NOT NULL,
  "update_time" DATE NOT NULL,
  CONSTRAINT "_COPY_2" PRIMARY KEY ("cart_item_id")
);
COMMENT ON COLUMN "shopping_cart"."cart_item_id" IS '购物项主键id';
COMMENT ON COLUMN "shopping_cart"."user_id" IS '用户主键id';
COMMENT ON COLUMN "shopping_cart"."goods_id" IS '关联商品id';
COMMENT ON COLUMN "shopping_cart"."goods_count" IS '数量(最大为5)';
COMMENT ON COLUMN "shopping_cart"."create_time" IS '创建时间';
COMMENT ON COLUMN "shopping_cart"."update_time" IS '最新修改时间';
```

![pict8](pict8.png)

##### 4、创建存储过程

a 加入购物车存储过程

```
--创建购物车信息流水号
CREATE SEQUENCE SALE.SEQ_CART INCREMENT BY 1 MINVALUE 1 MAXVALUE 3000 CYCLE CACHE 20 NOORDER;

--加入购物车存储过程
create or replace procedure ADD_CART(USER_ID IN NUMBER ,GOODS_ID IN NUMBER ,GOODS_COUNT IN NUMBER) 
AS 
SELECTED_GOODS_ID NUMBER;
SELECTED_GOODS_COUNT NUMBER;
begin
	SELECT "goods_id", "stock_num" INTO SELECTED_GOODS_ID, SELECTED_GOODS_COUNT FROM PROPERTY."goods_info" GI 
	WHERE GI."goods_id" = GOODS_ID AND GI."goods_sell_status" = 'Y' ;
	if(GOODS_COUNT>SELECTED_GOODS_COUNT) THEN
		dbms_output.put_line('商品库存不足！');
		RETURN;
	ELSE 
		UPDATE PROPERTY."goods_info" SET "stock_num" = "stock_num"- GOODS_COUNT;
		INSERT INTO SALE."shopping_cart" VALUES (SALE.SEQ_CART.NEXTVAL,USER_ID,GOODS_ID,GOODS_COUNT,SYSDATE,SYSDATE);
	END IF;
EXCEPTION
	WHEN NO_DATA_FOUND  THEN
	dbms_output.put_line('输入的商品ID：'||to_char(GOODS_ID)||'有误请确认！');
end ADD_CART;

BEGIN SALE.ADD_CART(1,1,11); END;
```

![pict9](pict9.png)

b 生成订单存储过程

```
--创建订单信息流水号
CREATE SEQUENCE SALE.SEQ_ORDER INCREMENT BY 1 MINVALUE 1 MAXVALUE 3000 CYCLE CACHE 20 NOORDER;
CREATE SEQUENCE SALE.SEQ_ORDER_ITEM INCREMENT BY 1 MINVALUE 1 MAXVALUE 3000 CYCLE CACHE 20 NOORDER;

--生成订单
create or replace procedure Generate_Order(USER_ID IN NUMBER) 
AS 
TOTAL_PRICE NUMBER;
ORDER_ID NUMBER;
BEGIN
	--计算订单总价
	SELECT SUM(GI."price" * SC."goods_count") INTO TOTAL_PRICE
	FROM SALE."shopping_cart" SC
	INNER JOIN PROPERTY."goods_info" GI ON GI."goods_id" = SC."goods_id" 
	WHERE SC."user_id" = USER_ID;
	--生成订单号
	SELECT SALE.SEQ_ORDER.NEXTVAL INTO ORDER_ID FROM DUAL;
	--插入订单主表信息
	INSERT INTO PROPERTY."order"
	VALUES (ORDER_ID, USER_ID, TOTAL_PRICE, NULL, 0 ,SYSDATE ,SYSDATE);
	--插入订单从表信息
	INSERT INTO PROPERTY."order_item"
	SELECT SALE.SEQ_ORDER_ITEM.NEXTVAL ,ORDER_ID ,"goods_id" ,"price" ,"goods_count" ,SYSDATE
	FROM
	(SELECT SC."goods_id" ,GI."price" ,SUM(SC."goods_count") "goods_count"
	FROM SALE."shopping_cart" SC
	INNER JOIN PROPERTY."goods_info" GI ON GI."goods_id" = SC."goods_id" 
	WHERE SC."user_id" = USER_ID
	GROUP BY SC."goods_id", GI."price" );
	--清除购物车信息
	DELETE FROM SALE."shopping_cart" WHERE "user_id" = USER_ID;
EXCEPTION
	WHEN NO_DATA_FOUND  THEN
	dbms_output.put_line('根据输入的用户ID：'||to_char(USER_ID)||'未找到选购的商品信息，请确认！');
end Generate_Order;

BEGIN SALE.Generate_Order(1); END;
```

![pict10](pict10.png)

c 取消订单和购物车存储过程

```
--取消订单和购物车
create or replace procedure CANCEL_SHOPPING(USER_ID IN NUMBER) 
AS 

BEGIN	
	--清除购物车信息
	DELETE FROM SALE."shopping_cart" WHERE "user_id" = USER_ID;
	--标记订单信息为取消
	UPDATE PROPERTY."order" SET "order_status" = 2, "update_time" = SYSDATE  
	WHERE "user_id" = USER_ID AND "order_status" = 0;
end CANCEL_SHOPPING;

BEGIN SALE.CANCEL_SHOPPING(1); END;
```

![pict11](pict11.png)

d 订单支付存储过程

```
--订单支付
create or replace procedure ORDER_PAY(USER_ID IN NUMBER) 
AS 
ORDER_ID NUMBER;
BEGIN
	SELECT "order_id" INTO ORDER_ID FROM PROPERTY."order" WHERE "user_id" = USER_ID AND "order_status" = 0;
	--清除购物车信息
	DELETE FROM SALE."shopping_cart" WHERE "user_id" = USER_ID;
	--标记订单信息为取消
	UPDATE PROPERTY."order" SET "order_status" = 1, "pay_time" = SYSDATE , "update_time" = SYSDATE 
	WHERE "user_id" = USER_ID AND "order_status" = 0;
EXCEPTION
	WHEN NO_DATA_FOUND THEN
	dbms_output.put_line('根据输入的用户ID：'||to_char(USER_ID)||'未找到订单信息，请确认！');
end ORDER_PAY;

BEGIN SALE.ORDER_PAY(1); END;
```

![pict12](pict12.png)

##### 5、备份方案

启动RMAN：在数据库服务器上打开终端或命令提示符，并键入以下命令以启动RMAN：
rman target /
这将连接到默认的本地目标数据库。

显示配置设置：一旦进入RMAN命令行界面，您可以使用以下命令来显示RMAN的当前配置设置：
SHOW ALL;
这将显示RMAN的所有配置设置和参数。

![pict13](pict13.png)

确定备份策略：根据业务需求和数据重要性确定备份频率和保留期限。一般而言，每日全量备份和定期增量备份是常见的选择。

数据库备份：使用Oracle提供的备份工具（如RMAN）执行数据库备份操作。全量备份可以定期进行，而增量备份则应该在全量备份之后进行，以记录自上次全量备份以来的更改。

全量备份：执行全量备份以捕获整个数据库的状态。全量备份可以在每日或每周等时间间隔内进行，具体取决于业务需求和数据增长速度。全量备份会生成数据库的初始快照，作为恢复的基础。

```
RUN {
  ALLOCATE CHANNEL ch1 DEVICE TYPE disk;
  BACKUP AS BACKUPSET DATABASE;
  RELEASE CHANNEL ch1;
}
```

增量备份：在完成全量备份后，执行增量备份以记录自上次全量备份以来的更改。增量备份只会备份数据库中发生变化的部分，可以大大减少备份时间和存储空间的需求。

```
RUN {
  ALLOCATE CHANNEL ch1 DEVICE TYPE disk;
  BACKUP INCREMENTAL LEVEL 1 DATABASE;
  RELEASE CHANNEL ch1;
}
```

### 实验总结

在本次实验中，完成了基于Oracle数据库的商品销售系统的数据库设计方案。以下是实施的主要步骤和实验结果的总结：

1. 表及表空间设计方案：设计了两个表空间，并创建了四张表。这些表包括goods_info表、order表、order_item表和shopping_cart表，生成了10万条的模拟商品信息数据。通过良好的表和表空间设计，我们可以有效地组织和管理系统的数据，并提高查询和维护的效率。
2. 权限及用户分配方案：设计了两个用户，分别是PROPERTY用户和sale用户。通过合理的权限分配，我们实现了数据的安全性和访问控制。
3. 存储过程设计：在创建了四个存储过程，分别是加入购物车存储过程、生成订单存储过程、取消订单和购物车存储过程和订单支付存储过程。通过封装业务逻辑和重复操作，我们提高了系统的效率和可维护性。
4. 数据库备份方案：设计了一套数据库备份方案，以确保数据的安全性和可恢复性。使用了Oracle提供的备份工具RMAN进行全量备份和增量备份操作。全量备份定期进行，而增量备份在全量备份之后执行，以记录自上次全量备份以来的更改。我们选择可靠的存储介质来存储备份数据，并定期测试备份的可恢复性。

通过本次实验，不仅深入了解了Oracle数据库的特点和优势，还学习了数据库设计、权限管理、存储过程和函数设计以及数据库备份等方面的知识和技能。通过这个实验，我更加熟悉了数据库管理工具和技术，并提升了自己在数据库设计和管理方面的能力。

通过实验过程中遇到的问题和挑战，学会了解决和应对各种数据库设计和管理方面的情况。我们意识到数据库管理在企业中的重要性，并明白了持续学习和实践的必要性，以跟上数据库技术的发展和变化。我将继续加强自己在数据库管理领域的学习，以提升自己在实践中的能力和技术水平。
