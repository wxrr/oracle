# 实验4：PL/SQL语言打印杨辉三角

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。

## 杨辉三角源代码

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
```

![pict1](pict1.png)

## 创建一个存储过程

```
-- 创建存储过程 YHTriangle
CREATE OR REPLACE PROCEDURE YHTriangle(N IN NUMBER) IS
    arr sys.odcinumberlist; -- 定义一个数组类型
BEGIN
    -- 初始化第一行
    arr := sys.odcinumberlist(1);

    -- 打印第一行
    DBMS_OUTPUT.PUT_LINE(arr(1));

    -- 逐行生成杨辉三角
    FOR i IN 2..N LOOP
        -- 在数组头尾添加1
        arr.EXTEND;
        arr(arr.LAST) := 1;

        -- 生成当前行
        FOR j IN 2..arr.COUNT-1 LOOP
            arr(j) := arr(j) + arr(j-1);
        END LOOP;

        -- 在数组尾添加1
        arr.EXTEND;
        arr(arr.LAST) := 1;

        -- 打印当前行
        FOR j IN 1..arr.COUNT LOOP
            DBMS_OUTPUT.PUT(arr(j) || ' ');
        END LOOP;
        DBMS_OUTPUT.NEW_LINE();
    END LOOP;
END;
```

![pict2](pict2.png)

## 打印出 10 行杨辉三角

```
-- 调用存储过程并传递行数参数
BEGIN
    YHTriangle(10);
END;
```

![pict3](pict3.png)

## 实验总结

- 存储过程的创建：通过使用CREATE OR REPLACE PROCEDURE语句，我们可以创建一个存储过程。在存储过程中，可以定义输入参数和局部变量，并编写实现代码。
- 数组类型的使用：在本次实验中，我们使用了sys.odcinumberlist数组类型来存储杨辉三角的每一行。通过在存储过程中定义这个数组类型的变量，我们可以方便地操作数组元素。

- 杨辉三角的生成：根据杨辉三角的特性，我们通过循环和数组操作来逐行生成杨辉三角。每一行的元素通过前一行的元素计算得出，并将结果存储在数组中。通过逐行生成并打印杨辉三角，我们可以在控制台上看到完整的杨辉三角形状。
